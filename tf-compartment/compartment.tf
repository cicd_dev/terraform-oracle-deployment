resource "oci_identity_compartment" "tf-compartment" {
    # Required
    compartment_id = "ocid1.tenancy.oc1..aaaaaaaahabf4ecjpejw4ukcpsh7j5un4bw5shr3fekfnus5fdtfdw4pl3xq"
    description = "Compartment for Terraform resources."
    name = "oracle4compatment"
}