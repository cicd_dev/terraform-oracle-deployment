provider "oci" {
  tenancy_ocid = "ocid1.tenancy.oc1..aaaaaaaahabf4ecjpejw4ukcpsh7j5un4bw5shr3fekfnus5fdtfdw4pl3xq"
  user_ocid = "ocid1.user.oc1..aaaaaaaadl4mot3mj6w4q2dzeo5ujpf2awqrk5at7lud6dlcr6b6chdvzlca" 
  private_key_path = "/home/juno/.oci/alex.pem"
  fingerprint = "dd:1c:a0:56:50:26:58:6d:ef:5e:92:b9:7d:17:94:07"
  region = "eu-amsterdam-1"
}