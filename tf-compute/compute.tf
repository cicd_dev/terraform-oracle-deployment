resource "oci_core_instance" "k3s_canonical" {
    # Required
    availability_domain = data.oci_identity_availability_domains.ads.availability_domains[0].name
    compartment_id = "ocid1.tenancy.oc1..aaaaaaaahabf4ecjpejw4ukcpsh7j5un4bw5shr3fekfnus5fdtfdw4pl3xq"
    #shape = "VM.Standard.E2.1.Micro"   #Free Tier (2)
    #shape = "VM.Standard3.Flex"
    shape = "VM.Standard.A1.Flex"  #arm64
    shape_config {
    memory_in_gbs = 12
    ocpus = 2
    }
    source_details {
        source_id = "ocid1.image.oc1.eu-amsterdam-1.aaaaaaaajkstjwjsbk5tkauk2qxirqybwvubossdp2mv2cvfcjb46rkxjlhq"
        #source_id = "ocid1.image.oc1.eu-amsterdam-1.aaaaaaaa6impx5efjblyqswpwlrgedcgh7rtdym3ejv2htnt4d7kk3odn2ta"  #Canonical-Ubuntu-22.04-Minimal-2023.10.15-0
        source_type = "image"
    }

    # Optional
    display_name = "arm64-12gb-server"
    create_vnic_details {
        assign_public_ip = true
        subnet_id = "ocid1.subnet.oc1.eu-amsterdam-1.aaaaaaaaqtmtlowbozgmhmmyj6t23evqbqn3stywgltytuxvx5lt2faprooa"
    }
    metadata = {
        ssh_authorized_keys = file("/home/juno/.ssh/platforma.key.pub")
    } 
    preserve_boot_volume = false
}