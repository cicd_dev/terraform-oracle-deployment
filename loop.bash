#!/bin/bash
cd tf-compute
for i in {1..5}; do
    echo -n "This is a test in loop $i\n ";
    terraform apply -auto-approve
    sleep 900
done
cd -